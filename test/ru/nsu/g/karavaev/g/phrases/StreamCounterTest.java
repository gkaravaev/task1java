package ru.nsu.g.karavaev.g.phrases;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.*;

public class StreamCounterTest {
    private final StreamCounter counter = new StreamCounter(3, 4);
    @Test
    public void streamAPITest() {
        ByteArrayInputStream bais = new ByteArrayInputStream("a a a a a a a a\nb b b b b b b\nc c c c c c".getBytes());
        counter.setInputStream(bais);
        try {
            counter.streamAPIScan();
        } catch (IOException e) {
            fail("Test failed: " + e.toString());
        }

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        counter.printStreamAPI(baos);
        assertEquals(baos.toString().replaceAll("\r\n", "\n").trim(), "a a a (6)\nb b b (5)\nc c c (4)");
    }

}