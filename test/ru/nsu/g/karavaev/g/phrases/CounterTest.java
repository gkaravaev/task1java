package ru.nsu.g.karavaev.g.phrases;

import org.junit.Assert;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;



public class CounterTest {

    private final Counter counter = new Counter (3, 4);


    @Test
    public void scanTest() {
        ByteArrayInputStream bais = new ByteArrayInputStream("a a a a a a a a\nb b b b b b b\nc c c c c c".getBytes());
        counter.setInputStream(bais);
        counter.scan();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        counter.print(baos);
        assertEquals(baos.toString().replaceAll("\r\n", "\n").trim(), "a a a (6)\nb b b (5)\nc c c (4)");
    }
}