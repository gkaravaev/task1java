package ru.nsu.g.karavaev.g.phrases;

import org.junit.Assert;
import org.junit.Test;
import static junit.framework.TestCase.assertEquals;

import java.util.HashMap;



public class AccumulatorTest {

    private final Accumulator accum = new Accumulator(2);

    @Test
    public void accumulate() {
        assertEquals("line2", accum.accumulate("line1", "line2"));
    }

    @Test
    public void getAnswer() {
        HashMap<String, Integer> map;
        accum.accumulate("line1", "line2");
        map = accum.getAnswer();
        assertEquals("ab", "ab");
        Assert.assertEquals(1, (int) map.get("line1 line2"));
    }

}