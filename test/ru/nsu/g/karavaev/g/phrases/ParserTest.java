package ru.nsu.g.karavaev.g.phrases;

import org.junit.Assert;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class ParserTest {

    private final Parser parser = new Parser(new String[]{"-n", "10", "-m", "21", "file.txt", "-t"});

    @Test
    public void findArg() {
            assertEquals(21, parser.findArg("-m"));
            assertEquals(10, parser.findArg("-n"));
    }

    @Test
    public void findFilename() {
        assertEquals("file.txt", parser.findFilename());
    }

    @Test
    public void testExceptions() {
        try {
            parser.findArg("-xx");
        } catch (Exception e) {
            assertEquals("Argument \"-xx\" not found", e.getMessage());
        }
        try {
            parser.findArg("-t");
        } catch (Exception e) {
            assertEquals("Argument \"-t\" value not found", e.getMessage());
        }
    }
}