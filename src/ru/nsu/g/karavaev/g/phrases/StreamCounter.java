package ru.nsu.g.karavaev.g.phrases;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamCounter {
    private Integer phraseLength;
    private Integer repeats;
    private HashMap<String, Integer> phrases;
    private String filename;
    private InputStream inputStream;

    StreamCounter() {
        phrases = new HashMap<>();
        inputStream = System.in;
        filename = "-";
    }

    StreamCounter(Integer phraseLength, Integer repeats) {
        phrases = new HashMap<>();
        this.phraseLength = phraseLength;
        this.repeats = repeats;
        inputStream = System.in;
        filename = "-";
    }

    public void streamAPIScan() throws IOException {
        Stream<String> stream;
        if (!filename.equals("-")) {
            BufferedReader in = new BufferedReader(new FileReader(filename));
            stream = in.lines();
        } else {
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
            stream = in.lines();
        }
        stream = Arrays.stream(stream.collect(Collectors.joining(" ")).split(" "));
        Accumulator accumulator = new Accumulator(phraseLength);
        stream.reduce(accumulator::accumulate);
        phrases = accumulator.getAnswer();
    }

    public void printStreamAPI(OutputStream out) {
        PrintWriter writer = new PrintWriter(out);
        phrases.entrySet()
                .stream()
                .sorted((e1, e2) -> -(e1.getValue().compareTo(e2.getValue())))
                .filter(x -> x.getValue() >= repeats)
                .forEach(x -> writer.println(x.getKey() + " (" + x.getValue() + ")"));
        writer.flush();
        writer.close();
    }

    public void setPhraseLength(Integer phraseLength) {
        this.phraseLength = phraseLength;
    }


    public void setRepeats(Integer repeats) {
        this.repeats = repeats;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

}
