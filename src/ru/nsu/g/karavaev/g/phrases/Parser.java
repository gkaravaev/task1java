package ru.nsu.g.karavaev.g.phrases;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class Parser {
    private String[] args;

    public Parser(String[] cmd) {
        this.args = cmd;
    }

    public int findArg(String argument) throws IllegalArgumentException, IndexOutOfBoundsException {
        int position = Arrays.asList(args).indexOf(argument);
        if (position == -1) throw new IllegalArgumentException("Argument \"" + argument + "\" not found");
        if (args.length - 1 != position) {
            return Integer.parseInt(Arrays.asList(args).get(position + 1)); //NumberFormatException
        } else
            throw new IndexOutOfBoundsException("Argument \"" + argument + "\" value not found");
    }

    public String findFilename() {
        for (String arg : args) {
            if ((arg.startsWith("-") && arg.length() == 1) ||
                    (!(arg.startsWith("-")) && !arg.matches("\\d+(\\.\\d+)?")))
                return arg;
        }
        throw new NoSuchElementException("Filename not found");
    }
}
