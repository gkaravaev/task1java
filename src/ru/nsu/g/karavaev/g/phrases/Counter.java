package ru.nsu.g.karavaev.g.phrases;


import java.io.*;
import java.util.*;

public class Counter {
    private Integer phraseLength;
    private Integer repeats;
    private HashMap<String, Integer> phrases;
    private String filename;
    private InputStream inputStream;

    Counter() {
        phrases = new HashMap<>();
        inputStream = System.in;
        filename = "-";
    }

    Counter(Integer phraseLength, Integer repeats) {
        phrases = new HashMap<>();
        this.phraseLength = phraseLength;
        this.repeats = repeats;
        inputStream = System.in;
        filename = "-";
    }


    public void scan() {
        Scanner scanner;
        if (!filename.equals("-")) {
            try {
                setInputStream(new FileInputStream(filename));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        scanner = new Scanner(inputStream);
        ArrayList<String> buffer = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        while (scanner.hasNext()) {
            buffer.add(scanner.next());         //scanning words
            if (buffer.size() == phraseLength) {//if we scanned phrase
                for (String str : buffer) {
                    builder.append(str);
                    builder.append(" ");
                }                               //add it to map
                int count = phrases.getOrDefault(builder.toString().trim(), 0);
                phrases.put(builder.toString().trim(), count + 1);
                buffer.remove(0);         //remove first element
                builder.setLength(0);
            }
        }
        scanner.close();
    }

    public void print(OutputStream out) {
        ArrayList<Map.Entry<String, Integer>> list = new ArrayList<>(phrases.entrySet());
        list.sort((e1, e2) -> -(e1.getValue().compareTo(e2.getValue())));
        PrintWriter writer = new PrintWriter(out);
        for (Map.Entry<String, Integer> phrase : list) {
            if (phrase.getValue() >= repeats)
                writer.println(phrase.getKey() + " (" + phrase.getValue() + ")");
        }
        writer.flush();
        writer.close();
    }


    public void setPhraseLength(Integer phraseLength) {
        this.phraseLength = phraseLength;
    }


    public void setRepeats(Integer repeats) {
        this.repeats = repeats;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }
}
