package ru.nsu.g.karavaev.g.phrases;

import java.util.ArrayList;
import java.util.HashMap;


public class Accumulator {
    private HashMap<String, Integer> accumulator;
    private ArrayList<String> buffer;
    private int length;
    Accumulator(){
        accumulator = new HashMap<>();
        buffer = new ArrayList<>();
    }
    Accumulator(int len) {
        buffer = new ArrayList<>();
        accumulator = new HashMap<>();
        length = len;
    }

    private void dropToMap(){
        StringBuilder builder = new StringBuilder();
        while (buffer.size() >= length) {
            for (int i = 0; i < length; i++) {
                builder.append(buffer.get(i)).append(" ");
            }
            int count = accumulator.getOrDefault(builder.toString().trim(), 0);
            accumulator.put(builder.toString().trim(), count + 1);
            buffer.remove(0);
            builder.setLength(0);
        }
    }

    public String accumulate (String line1, String line2) {
        if (buffer.isEmpty()) buffer.add(line1);
        buffer.add(line2);
        if (buffer.size() >= length)
            dropToMap();
        return line2;
    }

    public HashMap<String, Integer> getAnswer(){
        return accumulator;
    }
}