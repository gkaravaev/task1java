package ru.nsu.g.karavaev.g.phrases;

import java.io.IOException;
import java.util.NoSuchElementException;

public class Main {
    public static void main(String[] args) {
        Parser parser = new Parser(args);
        Counter counter = new Counter();
        try {
            String filename = parser.findFilename();
            if (filename.equals("-"))
                counter.setFilename("-");
            else
                counter.setFilename(filename);
        } catch (NoSuchElementException e) {
            //System.out.println(e.getMessage());
            counter.setFilename("-");
        }
        try {
            counter.setPhraseLength(parser.findArg("-n"));
        } catch (Exception e) {
            //System.out.println(e.getMessage());
            counter.setPhraseLength(2);
        }
        try {
            counter.setRepeats(parser.findArg("-m"));
        } catch (Exception e) {
            //System.out.println(e.getMessage());
            counter.setRepeats(2);
        }
        //counter.scan();
        counter.scan();

        counter.print(System.out);
    }
}